<?php

/**
 * @file
 * Supplies elasticsearch singleton object.
 */
namespace Drupal\elasticsearch_singleton;
use Elasticsearch\ClientBuilder;

class ElasticSingleton {

  private static $instance = NULL;

  /**
   * Checks connectivity with ElasticSearch.
   */
  private static function getClient() {
    try {
      $elastic_host = '127.0.0.1'; // Elasticsearch server.
      $client = ClientBuilder::create()
      ->setHosts([$elastic_host])
      ->build();
      $health = $client->cluster()->health();
      if (empty($health)) {
        return FALSE;
      }
      return $client;
    }
    catch (\Exception $e) {
      return FALSE;
    }
  }

  /**
   * Check connectivity getInstance.
   */
  public static function getInstance() {
    if(self::$instance === NULL) {
      self::$instance = self::getClient();
    }
    return self::$instance;
  }

}
