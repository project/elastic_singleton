Please make sure this module has been downloaded using composer.

How to install this module?
---------------------------
1. Install this module by drush command - drush en elasticsearch_singleton
2. Or install by "Extend" menu. (Drupal Admin Backend)

How to use this module?
-----------------------
This module offers two services for getting singleton object.
1. Direct object service
  \Drupal::service('elasticsearch_singleton.direct_object')::getInstance();
2. Indirect object service
  \Drupal::service('elasticsearch_singleton.indirect_object')->getInstance();

Both services provides singleton object.
