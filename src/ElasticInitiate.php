<?php

/**
 * @file
 * Provides indirect object of elasticsearch.
 */
namespace Drupal\elasticsearch_singleton;
use Drupal\elasticsearch_singleton\ElasticSingleton;

class ElasticInitiate {

/**
 * Singleton object.
 */
  public function getInstance() {
    return ElasticSingleton::getInstance();
  }

}
