<?php

namespace Drupal\elasticsearch_singleton\Controller;
use Drupal\Core\Controller\ControllerBase;

/**
 * An Elasticsearch singleton object testing controller.
 */
class ElasticController extends ControllerBase {

  /**
   * Returns a render-able array for a test page.
   */
  public function test() {
    $client = \Drupal::service('elasticsearch_singleton.direct_object')::getInstance();
    var_dump($client);
    $client = \Drupal::service('elasticsearch_singleton.indirect_object')->getInstance();
    var_dump($client);
    // exit(); // Uncomment this line to check both the client objects.
    $build = [
      '#markup' => $this->t('Elasticsearch Singleton example.'),
    ];
    return $build;
  }

}
